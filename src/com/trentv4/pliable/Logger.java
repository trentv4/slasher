package com.trentv4.pliable;

import static com.trentv4.pliable.LogLevel.INIT_NOTE;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * Principal logging class. Contains only two methods:
 * <ul>
 * <li><i>initialize(LogLevel)</i>, which initializes the system. Calling this multiple times will
 * 									erase all previous log messages.
 * <li><i>log(LogLevel, Object)</i>, which will print to console "<i>[STRING] Object</i>".</li>
 * <li><i>log(Object)</i>, which will do the former but always prefaced with [ALERT].</li>
 * <li><i>saveLog(String)</i>, which will save all log messages to disk.</li>
 * </ul>
 */
public final class Logger
{
	private static ArrayList<String> logMessages;
	private static ArrayList<LogLevel> logLevels;
	private static LogLevel logLevel;

	/** Private constructor so this class cannot be instantiated. */
	private Logger() {}; 
	
	/** Initializes the logging system at <i>loggingLevel</i>, where any log messages short of that
	 * level will be saved to disk but not shown in console. */
	public static final void initialize(LogLevel loggingLevel)
	{
		logMessages = new ArrayList<String>();
		logLevels = new ArrayList<LogLevel>();
		logLevel = loggingLevel;
		log(INIT_NOTE, "Logging system initialized");
	}
	
	/** Prints a log message with a where <i>message</i> will be designated <i>Important</i> level.*/
	public static final void log(Object message)
	{
		String messagec = message.toString();
		logMessages.add(messagec);
		logLevels.add(LogLevel.IMPORTANT);
		if(isLoggable(LogLevel.IMPORTANT))
		{
			System.out.println("<IMPORTANT> " + message);
		}
	}
	
	/** Prints a log message using string formatting **/
	public static final void log(LogLevel loggingLevel, String formatMessage, Object... formatParams)
	{
		log(loggingLevel, String.format(formatMessage, formatParams));
	}
	
	/** Prints a log message with a custom <i>level</i> that <i>message</i> will be prefaced with.
	 * <br><br>
	 * For example, <i>log(LogLevel.NOTE, "Never gonna give you up")</i> will print 
	 * <ul>"<i>[NOTE] Never gonna give you up</i>"</ul> to console.*/
	public static final void log(LogLevel loggingLevel, String message)
	{
		logMessages.add(message);
		logLevels.add(loggingLevel);
		
		if(isLoggable(loggingLevel))
		{
			System.out.println(String.format("<%s> %s", loggingLevel.toString(), message));
		}
	}
	
	/** Prints a log message with a custom <i>level</i> that <i>message</i> will be prefaced with.
	 * <br><br>
	 * For example, <i>log(LogLevel.NOTE, "Never gonna give you up")</i> will print 
	 * <ul>"<i>[NOTE] Never gonna give you up</i>"</ul> to console.*/
	public static final void log(LogLevel loggingLevel, Object... message)
	{
		for(int i = 0; i < message.length; i++)
		{
			logMessages.add(message[i].toString());
			logLevels.add(loggingLevel);
			
			if(isLoggable(loggingLevel))
			{
				System.out.println(String.format("<%s> %s", loggingLevel.toString(), message));
			}
		}
	}

	/** Writes a new file, saving all previous log messages to disk. */
	public static final void saveLog(String path)
	{
		try
		{
			new File(MainGame.getPath() + "log/").mkdirs();
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(MainGame.getPath() + "log/" + path)));
			for(int i = 0; i < logMessages.size(); i++)
			{
				writer.write(logMessages.get(i) + System.lineSeparator());
			}
			writer.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private static final boolean isLoggable(LogLevel loggingLevel)
	{
		return logLevel.compare(loggingLevel) != 1;
	}
}
