package com.trentv4.pliable;

import com.trentv4.slasher.GameStructureSlasher;


/** Primary loop class for the program. This class only exposes two methods: run() and draw(), called from MainGame and DisplayManager each.*/
public final class GameLoop
{
	private static boolean isInitialized;
	private static GameStructure gameStructure = new GameStructureSlasher();
	
	/** Executes one iteration of the game loop, or initializes it if it hasn't been. */
	public static final void run()
	{
		if(!isInitialized())
		{
			Texture.setLoadAlerts(false);
			AudioHandler.initialize();
			gameStructure.initialize();
			isInitialized = true;
		}
		if(DisplayManager.isInitialized())
		{
			InputMapper.tick();
			InputMapper.update();
			gameStructure.tick();
		}
	}
	
	/** Exposed draw function, called from the DisplayManager class. */
	public static final void draw()
	{
		gameStructure.draw();
	}
	
	/** Returns true if the GameStructure has been initialized. */
	public static boolean isInitialized()
	{
		return isInitialized;
	}
}
