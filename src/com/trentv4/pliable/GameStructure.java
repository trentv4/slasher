package com.trentv4.pliable;

/** Basic structure class. There should only be one of these, and the GameLoop.gameStructure should be set to it. */
public interface GameStructure
{
	/** Method called once before any ticks, used for initializing the game state. */
	public void initialize();
	/** Method called from DisplayManager, used for rendering the state. */
	public void draw();
	/** Primary state updater, called from MainGame. */
	public void tick();
}
