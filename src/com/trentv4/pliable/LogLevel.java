package com.trentv4.pliable;

/** Defines the level of the specified log message. */
public enum LogLevel
{
	/** An error message. Highly recommended to be visible. */
	ERROR(1),
	/** An important message. Recommended. */
	IMPORTANT(2), 
	/** A warning message. */
	WARNING(3),
	/** Not recommended, use for debugging only. */
	NOTE(4),
	/** Used by the engine for initialization output, use for initializing systems. */
	INIT_NOTE(0);
	
	private final int level;
	
	private LogLevel(int level)
	{
		this.level = level;
	}
	
	public int compare(LogLevel other)
	{
		return Integer.compare(level, other.level);
	}
}
