package com.trentv4.pliable;

import static com.trentv4.slasher.GameStructureSlasher.player;

import org.lwjgl.glfw.GLFW;

import com.trentv4.slasher.GameStructureSlasher;
import com.trentv4.slasher.entity.Entity;

/** Input class for all keyboard and mouse inputs. This class is called from DisplayManager, where it receives events from
 * the glfw window. This class has several important methods:
 * <ul>
 * <li><i>initialize()</i>, which will initialize all variables,</li> 
 * <li><i>setMousePos(x, y)</i>, which will set the internal variables to {x,y},</li> 
 * <li><i>setMouseUp(key)</i>, which sets a tracked mouse button as "up" or false,</li> 
 * <li><i>setMouseDown(key)</i>, which sets a tracked mouse button as "down" or true,</li> 
 * <li><i>tick()</i>, which is called from GameLoop once per iteration,</li> 
 * <li><i>setUp(key)</i>, which sets a tracked key as "up" or false,</li> 
 * <li><i>setDown(key)</i>, which sets a tracked key as "down" or true,</li> 
 * <ul>
 * */
public class InputMapper
{
	private static int[] mousePos = new int[2];
	private static boolean[] mouseButtons = new boolean[10];
	private static int wheelDelta = 0;
	
    private static boolean[] keyTickStatus = new boolean[348];
    private static boolean[] keyTickStatusPrevious = new boolean[348];	
    private static boolean[] keyStatus = new boolean[348];	
    
	/** Sets the current mouse position to {x, y}. */
	public static final void setMousePos(int x, int y)
	{
		mousePos[0] = x;
		mousePos[1] = y;
	}
	
	public static final void setMouseWheelDelta(int delta)
	{
		wheelDelta = delta;
	}
	
	/** Sets the provides <i>button</i> to "true". Used for the mouse.` */
	public static final void setMouseDown(int button)
	{
		if(mouseButtons.length >= button)
		{
			mouseButtons[button] = true;
		}
	}

	/** Sets the provides <i>button</i> to "false". */
	public static final void setMouseUp(int button)
	{
		if(mouseButtons.length >= button)
		{
			mouseButtons[button] = false;
		}
	}
	
	public static final int getWheel()
	{
		return wheelDelta;
	}
	
	public static final int[] getMousePos()
	{
		return mousePos;
	}
	
	public static final void update()
	{
		keyTickStatusPrevious = keyTickStatus;
		keyTickStatus = new boolean[131];
	}
	
	public static final boolean isPressed(int key)
	{
		return keyTickStatus[key] == true && keyTickStatusPrevious[key] == false;
	}
	
	public static final boolean isDown(int key)
	{
		return keyStatus[key];
	}
	
	/** Update method. This is called from GameLoop, where it runs every iteration of the logic thread. */
	public static final void tick()
	{
		if(mouseButtons[0])
		{
			mouseButtons[0] = false;
			Entity.getEntity(Entity.slash);
			Entity[] entities = GameStructureSlasher.entityList.toArray(new Entity[GameStructureSlasher.entityList.size()]);
			for(Entity e : entities)
			{
				if(Math.abs((e.x - player.x) + (e.y - player.y)) < 50)
				{
					if(e.UUID != player.UUID)
					{
						e.dealDamage(2);
					}
				}
			}
		}
		
		if(isDown(GLFW.GLFW_KEY_W))
		{
			player.addVectors(0,2);
		}
		if(isDown(GLFW.GLFW_KEY_S))
		{
			player.addVectors(0,-2);
		}
		if(isDown(GLFW.GLFW_KEY_A))
		{
			player.addVectors(-2,0);
		}
		if(isDown(GLFW.GLFW_KEY_D))
		{
			player.addVectors(2,0);
		}
		if(isPressed(GLFW.GLFW_KEY_SPACE))
		{
			player.addVectorsOverride(0,10);
		}
	}
	
	public static final void setStatus(int key, boolean state)
	{
		if(!keyTickStatus[key]) keyTickStatus[key] = true;
		keyStatus[key] = state;
	}
}
