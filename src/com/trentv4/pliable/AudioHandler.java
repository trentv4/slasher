package com.trentv4.pliable;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

public final class AudioHandler
{
	private static Clip musicClip;
	
	/** Creates the AudioHandler, including the musicClip system. */
	public static void initialize()
	{
		Logger.log(LogLevel.INIT_NOTE, "Initializing audio handler...");
		try
		{
			musicClip = AudioSystem.getClip();
		} catch (LineUnavailableException e) {
			Logger.log("Unable to get clip from AudioSystem! Crashing...");
			MainGame.crash(e);
		}
		Logger.log(LogLevel.INIT_NOTE, "Audio handler initialized!");
	}
	
	/** Sets the current music clip to path. There may be only one music clip. */
	public static final void setMusic(String path)
	{
		try
		{
			musicClip.stop();
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File("sounds/" + path));
			musicClip = AudioSystem.getClip();
			musicClip.open(audioIn);
			musicClip.start();
		} catch (Exception e) {
			Logger.log(LogLevel.ERROR, "Unable to set music: " + path);
		}
	}
	
	/** Plays a sound clip located at path. This may be called any number of times without overlapping. */
	public static final void playSound(String path)
	{
		try
		{
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File("sounds/" + path));
			Clip clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
		} catch (Exception e) {
			Logger.log(LogLevel.ERROR, "Unable to play sound: " + path);
		}
	}
}
