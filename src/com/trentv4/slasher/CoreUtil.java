package com.trentv4.slasher;

import com.trentv4.slasher.entity.Entity;
import com.trentv4.slasher.terrain.Terrain;

public abstract class CoreUtil
{
	public static final boolean checkTerrainCollision(int x, int y)
	{
		Terrain[] a = GameStructureSlasher.terrainList.toArray(new Terrain[GameStructureSlasher.terrainList.size()]);
		for(Terrain t : a)
		{
			if(x > t.x)
			{
				if(x < t.x+t.xSize)
				{
					if(y > t.y)
					{
						if(y < t.y+t.ySize)
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public static final boolean checkTerrainCollision(Entity e, int vecX, int vecY)
	{
		int x = e.x + vecX;
		int y = e.y + vecY;
		Terrain[] a = GameStructureSlasher.terrainList.toArray(new Terrain[GameStructureSlasher.terrainList.size()]);
		for(Terrain t : a)
		{
			if(x+e.xSize > t.x)
			{
				if(x < t.x+t.xSize)
				{
					if(y+e.ySize > t.y)
					{
						if(y < t.y+t.ySize)
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}
