package com.trentv4.slasher.terrain;

import java.util.ArrayList;

import com.trentv4.pliable.DisplayManager;
import com.trentv4.pliable.LogLevel;
import com.trentv4.pliable.Logger;
import com.trentv4.slasher.GameStructureSlasher;

public class Terrain
{
	public int x;
	public int y;
	public int vecX;
	public int vecY;
	
	public int xSize;
	public int ySize;
	public String texture;
	
	
	public int terrainId;
	public int UUID;
	
	private static int _maxTerrainId = 0;
	private static int _maxUUID = 0;

	private static ArrayList<Terrain> terrainTypeList = new ArrayList<Terrain>();
	
	public static final Terrain box = new Terrain().setTexture("box.png");
	
	private Terrain()
	{
		this.terrainId = _maxTerrainId;
		_maxTerrainId++;
		terrainTypeList.add(this);
	}
	
	public Terrain setTexture(String texture)
	{
		this.texture = texture;
		return this;
	}
	
	public Terrain setPos(int x, int y)
	{
		this.x = x;
		this.y = y;
		return this;
	}
	
	public Terrain setSize(int x, int y)
	{
		this.xSize = x;
		this.ySize = y;
		return this;
	}
	
	public Terrain setVectors(int x, int y)
	{
		this.vecX = x;
		this.vecY = y;
		return this;
	}

	public Terrain addVectors(int x, int y)
	{
		this.vecX += x;
		this.vecY += y;
		return this;
	}

	public static final Terrain getTerrain(Terrain entity)
	{
		return getTerrain(entity.terrainId);
	}
	
	public static final Terrain getTerrain(int id)
	{
		Terrain t = new Terrain();
		if(!(id >= terrainTypeList.size()))
		{
			Terrain base = terrainTypeList.get(id);
			
			//Assign ids
			t.terrainId = base.terrainId;
			t.UUID = _maxUUID;
			_maxUUID++;
			
			//Copy traits
			t.texture = base.texture;
			
			GameStructureSlasher.terrainList.add(t);
		}
		else
		{
			Logger.log(LogLevel.ERROR, "Unable to get entity: " + id);
		}
		return t;
	}
	
	public void tick()
	{
		this.x += vecX;
		this.y += vecY;
	}
	
	public void render()
	{
		DisplayManager.drawImage(texture, x, y, xSize, ySize);
	}
}
