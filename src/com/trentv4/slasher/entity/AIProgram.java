package com.trentv4.slasher.entity;

public interface AIProgram
{
	public void invoke(Entity e);
}
