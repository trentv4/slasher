package com.trentv4.slasher.entity;

import java.util.ArrayList;

import com.trentv4.pliable.DisplayManager;
import com.trentv4.pliable.LogLevel;
import com.trentv4.pliable.Logger;
import com.trentv4.slasher.CoreUtil;
import com.trentv4.slasher.GameStructureSlasher;

public class Entity
{
	public int x;
	public int y;
	public int vecX;
	public int vecY;
	public boolean isAlive = true;
	private int lifetime = 0;
	public int hp;
	public double angle = 0;
	
	
	public int maxLifetime;
	public int maxVec;
	public String texture;
	public int xSize;
	public int ySize;
	public AIProgram program;
	public int maxHP = 10;
	
	
	public int entityId;
	public int UUID;
	
	private static int _maxEntityId = 0;
	private static int _maxUUID = 0;

	private static ArrayList<Entity> entityTypeList = new ArrayList<Entity>();
	
	public static final Entity player = new Entity().setTexture("player.png").setMaxVec(6).setSize(50,50).setMaxHP(3);
	public static final Entity enemy = new Entity().setTexture("enemy.png").setMaxVec(4).setSize(50,50).setAIProgram(new AIProgramBasic());
	public static final Entity slash = new Entity().setTexture("slash.png").setLifetime(10).setMaxVec(4).setSize(100,100);
	
	public Entity()
	{
		this.entityId = _maxEntityId;
		_maxEntityId++;
		entityTypeList.add(this);
	}
	
	public Entity setTexture(String texture)
	{
		this.texture = texture;
		return this;
	}
	
	public Entity setMaxVec(int maxVec)
	{
		this.maxVec = maxVec;
		return this;
	}
	
	public Entity setMaxHP(int maxHP)
	{
		this.maxHP = maxHP;
		return this;
	}
	
	public Entity setPos(int x, int y)
	{
		this.x = x;
		this.y = y;
		return this;
	}
	
	public Entity setSize(int x, int y)
	{
		this.xSize = x;
		this.ySize = y;
		return this;
	}
	
	public Entity setAIProgram(AIProgram program)
	{
		this.program = program;
		return this;
	}
	
	public Entity setLifetime(int maxLifetime)
	{
		this.maxLifetime = maxLifetime;
		return this;
	}
	
	public Entity setVectors(int x, int y)
	{
		this.vecX = x;
		this.vecY = y;
		return this;
	}

	public Entity addVectorsOverride(int x, int y)
	{
		this.vecX += x;
		this.vecY += y;
		return this;
	}
	
	public Entity addVectors(int x, int y)
	{
		if(Math.abs(vecX + x) <= maxVec)
		{
			this.vecX += x;
		}
		else
		{
			if(x < 0) vecX = -maxVec;
			if(x > 0) vecX = maxVec;
		}
		
		if(Math.abs(vecY + y) <= maxVec)
		{
			this.vecY += y;
		}
		else
		{
			if(y < 0) vecY = -maxVec;
			if(y > 0) vecY = maxVec;
		}
		return this;
	}

	public static final Entity getEntity(Entity entity)
	{
		return getEntity(entity.entityId);
	}
	
	public static final Entity getEntity(int id)
	{
		Entity e;
		if(id == slash.entityId)
		{
			e = new EntitySlash();
		}
		else
		{
			e = new Entity();
			//ugly
		}
		if(!(id >= entityTypeList.size()))
		{
			Entity base = entityTypeList.get(id);
			
			//Assign ids
			e.entityId = base.entityId;
			e.UUID = _maxUUID;
			_maxUUID++;
			
			//Copy traits
			e.texture = base.texture;
			e.maxVec = base.maxVec;
			e.xSize = base.xSize;
			e.ySize = base.ySize;
			e.program = base.program;
			e.maxLifetime = base.maxLifetime;
			e.maxHP = base.maxHP;
			e.hp = e.maxHP;
			
			GameStructureSlasher.entityList.add(e);
		}
		else
		{
			Logger.log(LogLevel.ERROR, "Unable to get entity: " + id);
		}
		return e;
	}
	
	public void tick()
	{
		if(program != null)
		{
			program.invoke(this);
		}
		lifetime++;
		if(maxLifetime > 0)
		{
			if(lifetime >= maxLifetime)
			{
				this.kill();
			}
		}
		if(!CoreUtil.checkTerrainCollision(this, vecX, 0))
		{
			this.x += vecX;
		}
		else
		{
			if(vecX > 0)
			{
				for(int i = vecX; i > 0; i--)
				{
					if(!CoreUtil.checkTerrainCollision(this, i, 0))
					{
						this.x += i;
					}
				}
			}
			else
			{
				for(int i = vecX; i < 0; i++)
				{
					if(!CoreUtil.checkTerrainCollision(this, i, 0))
					{
						this.x += i;
					}
				}
			}
		}
		if(!CoreUtil.checkTerrainCollision(this, 0, vecY))
		{
			this.y += vecY;
		}
		else
		{
			if(vecY > 0)
			{
				for(int i = vecY; i > 0; i--)
				{
					if(!CoreUtil.checkTerrainCollision(this, 0, i))
					{
						this.y += i;
					}
				}
			}
			else
			{
				for(int i = vecY; i < 0; i++)
				{
					if(!CoreUtil.checkTerrainCollision(this, 0, i))
					{
						this.y += i;
					}
				}
			}
		}
		if(vecX > 0) vecX--;
		if(vecX < 0) vecX++;
		if(vecY > 0) vecY--;
		if(vecY < 0) vecY++;
	}
	
	public void dealDamage(int damage)
	{
		this.hp -= damage;
		textureSwitch = 1;
		if(hp <= 0)
		{
			kill();
		}
	}
	
	public void kill()
	{
		isAlive = false;
	}
	
	private int textureSwitch = 0;
	
	public void render()
	{
		if(textureSwitch > 0)
		{
			textureSwitch++;
			if(textureSwitch > 300)
			{
				textureSwitch = 0;
			}
			DisplayManager.drawImage(texture.substring(0,texture.length()-4)+"_damage.png", x, y, xSize, ySize);
		}
		else
		{
			DisplayManager.drawImage(texture, x, y, xSize, ySize);
		}
	}
}
