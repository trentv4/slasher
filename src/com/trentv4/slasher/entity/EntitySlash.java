package com.trentv4.slasher.entity;

import com.trentv4.pliable.DisplayManager;
import com.trentv4.slasher.GameStructureSlasher;

public class EntitySlash extends Entity
{
	protected EntitySlash()
	{
		super();
	}

	@Override
	public void tick()
	{
		super.tick();
		setPos(GameStructureSlasher.player.x-25, GameStructureSlasher.player.y-125);
	}
	
	@Override
	public void render()
	{
		DisplayManager.drawImage(texture, x, y, xSize, ySize, angle);
	}
}
