package com.trentv4.slasher;

import java.util.ArrayList;

import com.trentv4.pliable.DisplayManager;
import com.trentv4.pliable.GameStructure;
import com.trentv4.slasher.entity.Entity;
import com.trentv4.slasher.terrain.Terrain;

public class GameStructureSlasher implements GameStructure
{
	public static ArrayList<Entity> entityList = new ArrayList<Entity>();
	public static ArrayList<Terrain> terrainList = new ArrayList<Terrain>();
	public static Entity player;
	
	@Override
	public void initialize()
	{
		player = Entity.getEntity(Entity.player).setPos(100,100);
		Entity.getEntity(Entity.enemy).setPos(500,100);
		Terrain.getTerrain(Terrain.box).setPos(200,500).setSize(50,300);
		Terrain.getTerrain(Terrain.box).setPos(100,400).setSize(100,150);
		Terrain.getTerrain(Terrain.box).setPos(500,300).setSize(70,50);
		Terrain.getTerrain(Terrain.box).setPos(450,200).setSize(50,200);
		Terrain.getTerrain(Terrain.box).setPos(360,400).setSize(50,200);
		Terrain.getTerrain(Terrain.box).setPos(300,100).setSize(100,200);
	}

	@Override
	public void draw()
	{
		DisplayManager.drawImage("background.png", 0,0,800,600);
		Terrain[] terrain = GameStructureSlasher.terrainList.toArray(new Terrain[GameStructureSlasher.terrainList.size()]);
		for(Terrain t : terrain)
		{
			t.render();
		}
		Entity[] entities = GameStructureSlasher.entityList.toArray(new Entity[GameStructureSlasher.entityList.size()]);
		for(Entity e : entities)
		{
			if(e.isAlive) e.render();
		}
	}

	@Override
	public void tick()
	{
		for(Terrain t : terrainList)
		{
			t.tick();
		}
		for(Entity e : entityList)
		{
			if(e.isAlive) e.tick();
		}
	}
}
